#include "MyInterfaceTestSuite.h"


void MyInterfaceTestSuite::SetUp() {
    interface = (*GetParam())();
}

void MyInterfaceTestSuite::TearDown() {
    delete interface;
}

TEST_P(MyInterfaceTestSuite, GoodInputCanWriteToInterface) {
    // under GetParam() we will have instance of implemented interface
    EXPECT_TRUE(interface->write('a'));
}

TEST_P(MyInterfaceTestSuite, BadInputOnWriteReturnsFalse) {
    EXPECT_FALSE(interface->write('\0'));
}

TEST_P(MyInterfaceTestSuite, AfterWritingACharacterWeCanReadIt) {
    interface->write('w');
    ASSERT_EQ('w', interface->read());
}
