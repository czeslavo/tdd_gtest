#include "MyInterfaceTestSuite.h"
#include "InterfaceImplementation.h"

MyInterface* InterfaceImplementationFactory() {
    return new InterfaceImplementation();
}

INSTANTIATE_TEST_CASE_P(InterfaceImplementation1,
                        MyInterfaceTestSuite,
                        ::testing::Values(&InterfaceImplementationFactory));

