#ifndef EXAMPLES_MYINTERFACE_H
#define EXAMPLES_MYINTERFACE_H

#include <string>

struct MyInterface {
    virtual bool write(char character) = 0;
    virtual char read() = 0;

    virtual ~MyInterface() {}
};


#endif //EXAMPLES_MYINTERFACE_H
