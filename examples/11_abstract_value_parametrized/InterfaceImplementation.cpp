#include "InterfaceImplementation.h"


bool InterfaceImplementation::write(char character) {
    if (character != '\0') {
        buffer = character;
        return true;
    }
    else
        return false;

}

char InterfaceImplementation::read() {
    return buffer;
}

InterfaceImplementation::~InterfaceImplementation() {}
