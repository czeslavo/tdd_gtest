#ifndef EXAMPLES_MYINTERFACETESTSUITE_H
#define EXAMPLES_MYINTERFACETESTSUITE_H

#include <gtest/gtest.h>
#include "MyInterface.h"

typedef MyInterface* MyInterfaceFactory();

class MyInterfaceTestSuite : public ::testing::TestWithParam<MyInterfaceFactory*> {
protected:
    virtual void SetUp();
    virtual void TearDown();

    MyInterface* interface;
};


#endif //EXAMPLES_MYINTERFACETESTSUITE_H
