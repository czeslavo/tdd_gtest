#ifndef EXAMPLES_INTERFACEIMPLEMENTATION_H
#define EXAMPLES_INTERFACEIMPLEMENTATION_H

#include "MyInterface.h"

class InterfaceImplementation : public MyInterface {
public:
    virtual bool write(char character);

    virtual char read();
    virtual ~InterfaceImplementation();

protected:
    char buffer;
};

#endif //EXAMPLES_INTERFACEIMPLEMENTATION_H
