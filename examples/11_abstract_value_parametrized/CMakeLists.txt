cmake_minimum_required(VERSION 2.6)
project(11_abstract_value_parametrized)


set(SRC MyInterface.h
        MyInterfaceTestSuite.cpp
        MyInterfaceTestSuite.h
        InterfaceImplementation.cpp
        interface_implementation_tests.cpp InterfaceImplementation.h)

add_executable(11_tests ${SRC})
target_link_libraries(11_tests gtest pthread)
