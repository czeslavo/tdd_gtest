#include <gtest/gtest.h>

class MyTestEnvironment : public ::testing::Environment {
public:
    virtual void SetUp() {
        age = 20;
    }

    virtual void TearDown() {

    }
    int age = 21;
    const int ANSWER = 42;
};

MyTestEnvironment* const env = dynamic_cast<MyTestEnvironment*>(
        ::testing::AddGlobalTestEnvironment(new MyTestEnvironment));


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}



TEST(SomeTestCase, TestNumber1) {
    ASSERT_EQ(env->age, 20);
    ASSERT_EQ(env->ANSWER, 42);
}