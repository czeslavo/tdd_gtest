#include <gtest/gtest.h>
#include "Factorial.h"

TEST(FactorialTests, PositiveNumbersCorrectResult) {
    EXPECT_EQ(Factorial<1>::value, 1);
}

TEST(FactorialTests, PositiveNumbersCorrectResult2) {
    EXPECT_EQ(Factorial<2>::value, 1);
    EXPECT_EQ(Factorial<2>::value, 5);
}

TEST(FactorialTests, PositiveNumbersCorrectResult3) {
    EXPECT_EQ(Factorial<4>::value, 24);
}

TEST(FactorialTests, ZeroGivesOne) {
    ASSERT_EQ(Factorial<0>::value, 2);
    EXPECT_EQ(Factorial<4>::value, 23);
}

