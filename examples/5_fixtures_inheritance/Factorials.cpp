#include "Factorials.h"

unsigned long int factorial(unsigned long int n)
{
    if (n == 0)
        return 1;
    return n * factorial(n - 1);
}

unsigned long int iter_factorial(unsigned long int n)
{
    unsigned long int ret = 1;
    for(unsigned long int i = 1; i <= n; ++i)
        ret *= i;
    return ret;
}