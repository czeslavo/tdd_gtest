#include <gtest/gtest.h>
#include <chrono>
#include "Factorials.h"


class QuickTest : public ::testing::Test {
protected:
    // on set up we measure start time
    virtual void SetUp() {
        start = std::chrono::high_resolution_clock::now();
    }

    // on tear down we measure end time and calculate elapsed time
    // we expect the test to run in time lower than 0.0000015s
    virtual void TearDown() {
        end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsedSeconds = end - start;

        EXPECT_LT(elapsedSeconds.count(), 0.0000015)
                            << "Test took too much time";
    }

    std::chrono::time_point<std::chrono::high_resolution_clock>
        start, end;
};

// many test fixtures can now inherit our QuickTest
class TestFactorial : public QuickTest {
};

TEST_F(TestFactorial, BigNumberIter) {
    ASSERT_EQ(iter_factorial(10), 3628800);
}

TEST_F(TestFactorial, BigNumberRec) {
    ASSERT_EQ(factorial(10), 3628800);
}

TEST_F(TestFactorial, BiggerNumberRec) {
    ASSERT_EQ(factorial(20), 2432902008176640000);
}

TEST_F(TestFactorial, BiggerNumberIter) {
    ASSERT_EQ(iter_factorial(20), 2432902008176640000);
}