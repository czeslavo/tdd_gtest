#ifndef EXAMPLES_FACTORIALS_H
#define EXAMPLES_FACTORIALS_H

unsigned long int factorial(unsigned long int n);
unsigned long int iter_factorial(unsigned long int n);

#endif //EXAMPLES_FACTORIALS_H
