#include <signal.h>
#include <iostream>
#include <gtest/gtest.h>


// causes simulated normal exit
void normalExit() {
    std::cerr << "Success";
    exit(0);
}

// causes simulated seg fault
void segFault() {
    std::cerr << "Terminated by signal 11 (core dumped)";
    exit(11);
}

TEST(SomeDeathTest, NormalExit) {
    ASSERT_EXIT(normalExit(), ::testing::ExitedWithCode(0), "Success");
}

TEST(SomeDeathTest, SegFault) {
    ASSERT_EXIT(segFault(), ::testing::ExitedWithCode(11), "dumped");
}

// crash will be verified only if death tests are supported
TEST(SomeDeathTest, SegFault2) {
    EXPECT_DEATH_IF_SUPPORTED(segFault(), "dumped");
}

