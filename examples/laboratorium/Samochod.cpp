#include "Samochod.h"

// prosta implementacja zdefinowanego przez nas interfejsu Pojazd

bool Samochod::przemieszczajSie(unsigned dystans) {
    if (!zaparkowany) {
        przebieg += dystans;
        return true;
    }
    else
        return false;
}

unsigned Samochod::pokazLicznik() {
    return przebieg;
}

bool Samochod::zaparkuj() {
    if (!zaparkowany)
        zaparkowany = true;
}

bool Samochod::czyZaparkowany() {
    return zaparkowany;
}

bool Samochod::wlaczSieDoRuchu() {
    if (zaparkowany)
        zaparkowany = false;
}
