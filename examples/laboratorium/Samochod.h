#pragma once

#include "Pojazd.h"


class Samochod : public Pojazd {
public:
    virtual bool przemieszczajSie(unsigned distance);
    virtual unsigned pokazLicznik();
    virtual bool zaparkuj();
    virtual bool czyZaparkowany();
    virtual bool wlaczSieDoRuchu();

    virtual ~Samochod() {}

protected:
    unsigned przebieg;
    bool zaparkowany;
};

