#include "Samochod.h"
#include "PojazdTestSuite.h"

// fabryka samochodow, ktora przekazujemy jako parametr test case'a
Pojazd* fabrykaSamochodow() {
    return new Samochod();
}

// utworzenie instancji testow, na podstawie wczesniej zdefiniowanego wzoru
INSTANTIATE_TEST_CASE_P(SamochodowaInstancja,
                        PojazdTestSuite,
                        testing::Values(&fabrykaSamochodow));