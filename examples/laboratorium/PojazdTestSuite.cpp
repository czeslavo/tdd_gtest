#include <gtest/gtest.h>
#include "PojazdTestSuite.h"

void PojazdTestSuite::SetUp() {
    pojazd = (*GetParam())();
}

void PojazdTestSuite::TearDown() {
    delete pojazd;
}

TEST_P(PojazdTestSuite, PojazdMozeSiePrzemieszczac) {
    ASSERT_TRUE(pojazd->przemieszczajSie(1));
}

TEST_P(PojazdTestSuite, PojazdPoPrzejechaniuDystansuZwiekszaSwojPrzebieg) {
    unsigned staryPrzebieg = pojazd->pokazLicznik();
    pojazd->przemieszczajSie(20);

    ASSERT_GT(pojazd->pokazLicznik(), staryPrzebieg);
}

TEST_P(PojazdTestSuite, PojazdMoznaZaparkowac) {
    ASSERT_TRUE(pojazd->zaparkuj());
}

TEST_P(PojazdTestSuite, ZaparkowanyPojazdNieJezdzi) {
    pojazd->zaparkuj();

    ASSERT_FALSE(pojazd->przemieszczajSie(5));
}

TEST_P(PojazdTestSuite, ZaparkowanyPojazdInformujeOSwoimStanie) {
    pojazd->zaparkuj();

    ASSERT_TRUE(pojazd->czyZaparkowany());
}



