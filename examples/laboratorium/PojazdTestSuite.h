// Celem laboratorium jest zapoznanie się z użyciem frameworka
// Google Test do tworzenia testów jednostkowych.

// Proszę zadeklarować interfejs Pojazd, zawierający metody:
// * przemieszczajSie()
// * pokazLicznik()
// * zaparkuj()
// * wlaczSieDoRuchu()
// * czyZaparkowany()
// Do powyższego interfejsu, proszę utworzyć TestSuite, będący wzorem dla testów, które musi
// przejść każda jego implementacja. Do utworzenia TestSuite proszę użyć testów parametryzowanych
// wartością - inaczej - abstrakcyjnych testów parametryzowanych wartością.
//
// Później, w celu sprawdzenia działania utworzonych wzorów testów, proszę zaimplementować
// interfejs Pojazd dla typu Samochod i przeprowadzić je dla niego.
//
// Uwaga: skompilowany wzorcowy TestSuite należy dolinkować do naszych właściwych testów.

#pragma once
#include "Pojazd.h"
#include <gtest/gtest.h>

// dla wygody definiujemy typ fabrykaPojazdow - funkcja, która zwraca Pojazd*
typedef Pojazd* fabrykaPojazdow();

// TestSuite dziedziczący po TestWithParam<T>, pozwalający nam na utworzenie abstrakcyjnego testu
class PojazdTestSuite : public ::testing::TestWithParam<fabrykaPojazdow*> {
protected:
    virtual void SetUp();
    virtual void TearDown();

    // pojazd bedzie generowany przez fabryke dla kazdego testu
    Pojazd* pojazd;
};


