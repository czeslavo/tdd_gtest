#pragma once

// interfejs pojazdu, posiada tylko czysto wirtualne metody
// oraz wirtualny destruktor
struct Pojazd {
    virtual bool przemieszczajSie(unsigned distance) = 0;
    virtual unsigned pokazLicznik() = 0;
    virtual bool zaparkuj() = 0;
    virtual bool wlaczSieDoRuchu() = 0;
    virtual bool czyZaparkowany() = 0;

    virtual ~Pojazd() {}
};
