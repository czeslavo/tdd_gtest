#include <string>
#include <iostream>
#include <gtest/gtest.h>
#include "MyArray.h"

// To use a test fixture, derive a class from testing::Test
class MyArrayTest : public ::testing::Test {
protected:
    // initialize some arrays for each test
    virtual void SetUp() {
        std::cout << "MyArrayTest.SetUp()" << std::endl;

        arrInt = new MyArray<int>( {5, 2, 1, 6, 21, 78, 0} );
        arrFloat = new MyArray<float>( {7.3, 12.1, 6.2, 2.3, 1.3} );
        arrFloat2 = new MyArray<float>( {7.3, 12.1, 6.2, 2.3, 1.315} );
        arrString = new MyArray<std::string>( {"c++", "python", "java"} );
        arrString2 = new MyArray<std::string>( {"c++", "python", "scala"} );
        arrString3 = new MyArray<std::string>( {"c++", "python", "scala"} );
    }

    // clean up after every test
    virtual void TearDown() {
        std::cout << "MyArrayTest.TearDown()" << std::endl;
        delete arrInt;
        delete arrFloat;
        delete arrFloat2;
        delete arrString;
        delete arrString2;
        delete arrString3;
    }

    MyArray<int>* arrInt;
    MyArray<float>* arrFloat;
    MyArray<float>* arrFloat2;
    MyArray<std::string>* arrString;
    MyArray<std::string>* arrString2;
    MyArray<std::string>* arrString3;
};

// When you have a test fixture, you define a test using TEST_F
// instead of TEST

TEST_F(MyArrayTest, PoppingIntValueChangesSizeByOne) {
    auto sizeBefore = arrInt->size();
    arrInt->pop();
    auto sizeAfter = arrInt->size();

    ASSERT_EQ(sizeBefore, sizeAfter + 1);
}

TEST_F(MyArrayTest, PoppingFloatValueChangesSizeByOne) {
    auto sizeBefore = arrFloat->size();
    arrFloat->pop();
    auto sizeAfter = arrFloat->size();

    ASSERT_EQ(sizeBefore, sizeAfter + 1);
}

TEST_F(MyArrayTest, AddingAndThenPoppingGivesTheSameValue) {
    arrString->add("c#");
    ASSERT_EQ(arrString->pop(), "c#");

}

TEST_F(MyArrayTest, DifferentArraysAreNotTheSame) {
    // not equal assert
    ASSERT_NE(arrString, arrString2);
}

TEST_F(MyArrayTest, SumsOfFloatArraysAreTheSame) {
    auto sum1 = arrFloat->sum();
    auto sum2 = arrFloat2->sum();

    // normal assert
    // ASSERT_EQ(sum1, sum2);

    // assert for float (4 units of least precision)
    // ASSERT_FLOAT_EQ(sum1, sum2);

    // assert for floats with given absolute difference
    ASSERT_NEAR(sum1, sum2, 0.2);
}

TEST_F(MyArrayTest, TheSameArraysAreTheSame) {
    // arrays aren't the same and the only message we get is their addresses
    ASSERT_EQ(*arrString2, *arrString3);

}
