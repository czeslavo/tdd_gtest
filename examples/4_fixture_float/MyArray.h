#ifndef EXAMPLES_MYARRAY_H
#define EXAMPLES_MYARRAY_H

#include <vector>
#include <initializer_list>
#include <algorithm>
#include <type_traits>

template <typename T>
class MyArray {
public:
    // we may initialize the array with initializer list
    MyArray(std::initializer_list<T> list) :
            arr(list)
    { }

    // we may initialize the array with given size and init value
    MyArray(const int size, const T& value) {
        arr.reserve(size);
        std::fill(arr.begin(), arr.begin() + size, value);
    }

    // adds element to the array
    void add(const T element) {
        arr.push_back(element);
    }

    // removes and returns the last element
    T pop() {
        T tempValue = arr.back();
        arr.pop_back();
        return tempValue;
    }

    // returns size of the array
    size_t size() {
        return arr.size();
    }

    // check if arrays are the same
    bool operator ==(const MyArray<T> array) const {
        if (std::distance(arr.begin(), arr.end()) == std::distance(array.arr.begin(), array.arr.end())) {
            for (auto i = 0; i < arr.size(); ++i) {
                if (arr[i] != array.arr[i])
                    return false;
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    // if type T is arithmetic, calculates a sum of all elements
    T sum() {
        assert(std::is_arithmetic<T>::value);
        typename std::vector<T>::value_type sum = 0;
        for (auto element : arr) {
            sum += element;
        }
    }

    typename std::vector<T>::const_iterator begin() {
        return arr.begin();
    }

    typename std::vector<T>::const_iterator end() {
        return arr.end();
    }


private:
    std::vector<T> arr;
};



#endif //EXAMPLES_MYARRAY_H
