#include <gtest/gtest.h>
#include <list>


template <typename T>
class TypedTest : public ::testing::Test {
public:
    typedef std::list<T> List;
    static T shared;
    T value;
};

template<typename T>
T TypedTest<T>::shared;

typedef ::testing::Types<char, int, unsigned int> MyTypes;
TYPED_TEST_CASE(TypedTest, MyTypes);

TYPED_TEST(TypedTest, TestNumber1) {
    // Inside a test, refer to the special name TypeParam to get the type
    // parameter.  Since we are inside a derived class template, C++ requires
    // us to visit the members of FooTest via 'this'.
    TypeParam n = this->value;

    // To visit static members of the fixture, add the 'TestFixture::'
    // prefix.
    n += TestFixture::shared;

    // To refer to typedefs in the fixture, add the 'typename TestFixture::'
    // prefix.  The 'typename' is required to satisfy the compiler.
    typename TestFixture::List values;
    values.push_back(n);
}