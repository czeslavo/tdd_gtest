#include <vector>
#include <map>
#include <gtest/gtest.h>


TEST(BuiltinsTest, Test1) {
    std::vector<std::string> v1 = {"cpp", "java", "scala"};
    std::vector<std::string> v2 = {"cpp", "java", "pascal"};

    typedef std::pair<std::string, int> StrInt;
    std::map<std::string, int> m1 = {StrInt("cpp", 5), StrInt("scala", 3)};
    std::map<std::string, int> m2 = {StrInt("c", 5), StrInt("java", 3)};

    EXPECT_EQ(v1, v2);
    EXPECT_EQ(m1, m2);
}

