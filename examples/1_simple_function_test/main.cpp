#include "modul.h"
#include <iostream>
#include <assert.h>

// macros to give some feedback of tests
#define TEST_PASSED std::cout << "test passed" << std::endl
#define TEST_FAILED std::cout << "test failed" << std::endl


int main(int argc, char** argv) {
	// C assert - if it fails, std::abort is called and a message 
	// with failed assertion is sent to stdout
	assert (distance(1, 1, 1, 2) == 1);

	// it fails 
	// assert (distance(1, 1, 1, 5) == 1);

	// check if distance function works properly and give some feedback
	if (distance(1, 1, 1, 2) == 1) 
		TEST_PASSED;
	else
		TEST_FAILED;

	if (distance(1, 1, 0, 0) == 1)
		TEST_PASSED;
	else
		TEST_FAILED;
}
