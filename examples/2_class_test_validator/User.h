#pragma once
#include <string>


class User {
public:
	// creates an user with a given nickname and e-mail
	User(std::string nickname, std::string mail);

	// prints out a nickname and an e-mail
	std::string toString();

private:
	std::string nickname;
	std::string mail;

	// checks if an e-mail of the user is valid
	bool validMail(std::string);

	// checks if a nickname of the user is valid
	bool validNickname(std::string);
};