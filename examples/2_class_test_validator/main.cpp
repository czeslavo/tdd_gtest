#include "User.h"
#include <iostream>
#include <assert.h>

// macros to give some feedback of tests
#define TEST_PASSED std::cout << "test passed" << std::endl
#define TEST_FAILED std::cout << "test failed" << std::endl


int main(int argc, char** argv) {
	// check if user with proper nickname and e-mail is created properly
	User user1("mariusz94", "mariusz@gmail.com");

	if (user1.toString() == "mariusz94 mariusz@gmail.com")
		TEST_PASSED;
	else
		TEST_FAILED;

	// check if user with wrong nickname and e-mail is created 
	// in way we would like him to be created
	User userWrong("11mariusz", "mariusz.com");

	if (userWrong.toString() == "[invalid nickname] [invalid email]")
		TEST_PASSED;
	else
		TEST_FAILED;


	// there should be more tests
}
