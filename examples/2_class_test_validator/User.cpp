#include "User.h"
#include <regex>

User::User(std::string nickname, std::string mail) {
	if (validNickname(nickname))
		this->nickname = nickname;
	else
		this->nickname = "[invalid nickname]";

	if (validMail(mail))
		this->mail = mail;
	else
		this->mail = "[invalid email]";
}

std::string User::toString() {
	return this->nickname + " " + this->mail;
}

bool User::validNickname(std::string nickname) {
	std::regex nickPattern("[^0-9].+");
	if (std::regex_match(nickname, nickPattern))
		return true;
	else
		return false;
}

bool User::validMail(std::string mail) {
	std::regex mailPattern(".+@.+");
	if (std::regex_match(mail, mailPattern)) 
		return true;
	else
		return false;
}