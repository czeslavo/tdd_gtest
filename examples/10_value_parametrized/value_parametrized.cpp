#include <bitset>
#include <regex>

#include <gtest/gtest.h>


class PhoneNumberParametrizedTest : public ::testing::TestWithParam<std::string> {
    // here we can implement as usually SetUp() and TearDown() methods
public:
    static void SetUpTestCase() {
        phonePattern = std::regex("^\\+[0-9]{2} [0-9]{3}-[0-9]{3}-[0-9]{3}$");
    }

    static std::regex phonePattern;
};

std::regex PhoneNumberParametrizedTest::phonePattern;

////////////////////////////////////////////////////////////////////////////////////
// it's also possible to add parameter to existing test
class MyBaseTest : public ::testing::Test {

};

class MyBaseTestWithParameter : public MyBaseTest,
                                public ::testing::WithParamInterface<int> {

};
////////////////////////////////////////////////////////////////////////////////////


bool isCallable(std::string number) {
    static std::regex polishPhonePattern = std::regex("^\\+48 [0-9]{3}-[0-9]{3}-[0-9]{3}$");
    return std::regex_match(number, polishPhonePattern);
}

TEST_P(PhoneNumberParametrizedTest, ValidNumbersAreValid) {
    EXPECT_TRUE(std::regex_match(GetParam(), phonePattern));
}

TEST_P(PhoneNumberParametrizedTest, ValidPolishNumbersAreConsideredAsCallable) {
    EXPECT_TRUE(isCallable(GetParam()));
}

// instantiate each test patterns for each value - here we will instantiate 2 x 3 tests
INSTANTIATE_TEST_CASE_P(PolishNumbersInstantion,
                        PhoneNumberParametrizedTest,
                        ::testing::Values("+48 512-713-721", "+48 612-612-723", "+48 206-236-235"));

// their names will be:
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidNumbersAreValid/0
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidNumbersAreValid/1
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidNumbersAreValid/2
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidPolishNumbersAreConsideredAsCallable/0
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidPolishNumbersAreConsideredAsCallable/1
//PolishNumbersInstantion/PhoneNumberParametrizedTest.ValidPolishNumbersAreConsideredAsCallable/2


