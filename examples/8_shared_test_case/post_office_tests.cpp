#include "PostOffice.h"
#include <gtest/gtest.h>

class PostOfficeTest : public ::testing::Test {
protected:
    static void SetUpTestCase() {
        // some huge shared resources that won't change during tests
        // or if - their state will be recovered by changer
        polishGeneralPostOffice = new PostOffice({
                {"Kraków", "31-612"},
                {"Warszawa", "61-251"},
                {"Wrocław", "61-261"},
                {"Koniecpol", "67-125"}
                // ...
        } );
    }

    static void TearDownTestCase() {
        delete polishGeneralPostOffice;
    }

    // static field will be visible for each test and won't be initialized each time
    static PostOffice* polishGeneralPostOffice;
};

// remember to intialize static field
PostOffice* PostOfficeTest::polishGeneralPostOffice = nullptr;

TEST_F(PostOfficeTest, KrakowPostCodeIsProer) {
    auto code = polishGeneralPostOffice->getCodeForCity("Kraków");
    ASSERT_EQ("31-612", code);
}

TEST_F(PostOfficeTest, NotExistingCityRequestGivesProperMessage) {
    auto code = polishGeneralPostOffice->getCodeForCity("Wypogodzice");
    ASSERT_EQ("no such city in our database", code);
}