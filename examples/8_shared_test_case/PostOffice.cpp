#include "PostOffice.h"



PostOffice::PostOffice(std::initializer_list<pairType> list) :
    postalCodes(list)
{
}

std::string PostOffice::getCodeForCity(std::string cityName) const {
    auto cityIt = postalCodes.find(cityName);
    if (cityIt != postalCodes.end()) {
        return cityIt->second;
    }
    else
    {
        return "no such city in our database";
    }
}
