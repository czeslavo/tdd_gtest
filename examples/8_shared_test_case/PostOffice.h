#ifndef EXAMPLES_POSTOFFICE_H
#define EXAMPLES_POSTOFFICE_H

#include <map>
#include <string>
#include <initializer_list>

typedef std::map<std::string, std::string>::value_type pairType;

class PostOffice {
public:
    PostOffice(std::initializer_list<pairType> list);
    std::string getCodeForCity(std::string) const;
private:
    std::map<std::string, std::string> postalCodes;
};


#endif //EXAMPLES_POSTOFFICE_H
